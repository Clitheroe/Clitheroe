## Hello I'm Pete

Hello, I’m [Pete Clitheroe](https://www.linkedin.com/in/peterclitheroe83/). Husband, Father of two girls and Bengie the Cockapoo and we live near Manchester in the UK. My goal, [Ikigai](https://en.wikipedia.org/wiki/Ikigai) if you will, is to attain fulfillment from everything I undertake. This will is underpinned by three core values: Health, Freedom in life and Discipline in how I operate. 

I am immensely proud to work here at GitLab in the SMB division. It is you, colleagues within this company that inspire me to focus, think forward and contribute where I can to the mission of growing this company to be the greatest Software company we can be. 

**What am I good at**
- Listening, seeking to understand and helping to find a path forward against an actionable plan
- Building programmes of work that have definition and structure, and then executing them
- Finding a way to get stuff done to the highest quality I can, no matter what


**What am I bad at**
- Task management. I can never find an effective way to manage my task list
- Retaining ownership of too many topics. I am awful at delegating as I want it done my way - which is never the right way
- Coming up with too many ideas, too fast, to add to the ones I already have


**What am I working on**
- Increasing diversity of thought & perspective
- Understanding the impact of my actions 
- Understanding the potential of my potential and where my [Peter Principle](https://www.google.com/search?gs_ssp=eJzj4tDP1TcwM640MGD04i9ILUktUigoysxLzizISQUAa5EIsA&q=peter+principle&rlz=1C5CHFA_enGB988GB988&oq=Peter+princi&aqs=chrome.1.0i355i433i512j46i433i512j0i512j69i57j0i512l6.3891j0j7&sourceid=chrome&ie=UTF-8) may be


**Podcasts I listen too** (I use the App: [PocketCasts](https://pocketcasts.com/))

- Simon Sinek: A bit of Optimism. Provides me with insight into the World and work scenarios that allow me to think with a diverse opinion from my natural disposition
- Steven Bartlett: The diary of a CEO. Provides me with a mindset on how to handle tricky situations while learning how to be assertive with a stance and opinion
- Elizabeth Day: How to Fail. The way people have learnt from getting things wrong
- Jay Shetty: On Purpose. Thinking about the mind and how to retain balance in such a hectic World
- High Performance: How Sports and Business related individual structure their mindset to attain excellence while overcoming adversity 
- 30 Minutes to Presidents Club: Becoming inspired by the creative ways to address selling and the motions that can be relevant to me (Note: this is on Spotify)


**Impactful reads**

- [Stillness is the Key](https://www.google.com/search?gs_ssp=eJzj4tVP1zc0TK9MystON8w1YPQSKS7JzMnJSy0uVsgsVijJSFXITq0EAOWZDJU&q=stillness+is+the+key&rlz=1C5CHFA_enGB988GB988&oq=Stillness+is+the+&aqs=chrome.1.0i355i512j46i512j46i340i512l2j69i57j0i512l5.4638j0j15&sourceid=chrome&ie=UTF-8) by Ryan Holiday : Finding balance and perspective within your Mind, Body & Soul 
- [Moment of Lift](https://www.google.com/search?q=moment+of+lift&rlz=1C5CHFA_enGB988GB988&ei=otQ-ZNfkF9PugAaPzJ3oAg&gs_ssp=eJzj4tVP1zc0TMuoMijOyS0xYPTiy83PTc0rUchPU8jJTCsBAJ5RClQ&oq=moment+of+lif&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQAxgAMggILhCKBRCRAjIFCC4QgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDITCC4QigUQkQIQ3AQQ3QQQ4AQYAToHCC4QigUQQzoKCC4QigUQ1AIQQzoHCAAQigUQQzoLCAAQgAQQsQMQgwE6DgguEMcBELEDENEDEIAEOgsILhDUAhCxAxCABDoSCC4QigUQQxDcBBDeBBDgBBgBOg4ILhCABBCxAxDHARDRAzoICAAQgAQQsQM6EwguEIoFEJECENwEEN4EEOAEGAE6CwgAEIoFELEDEIMBOgoIABCABBCxAxAKOgoILhCxAxCABBAKOggILhCABBCxAzoICC4QsQMQgAQ6DgguEIAEELEDEIMBENQCSgQIQRgAUABYkgpgxBdoAHABeACAAW6IAb0IkgEEMTEuMpgBAKABAcABAdoBBggBEAEYFA&sclient=gws-wiz-serp) by Melinda Gates: What empowerment can really look like in action 
- [Daring Greatly](https://www.google.com/search?q=daring+greatly&rlz=1C5CHFA_enGB988GB988&ei=wtQ-ZPzPKojdgAa1wZS4Bw&ved=0ahUKEwj8uN_P-7P-AhWILsAKHbUgBXcQ4dUDCA8&uact=5&oq=daring+greatly&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQAzILCC4QsQMQigUQkQIyCAguEIoFEJECMggIABCKBRCRAjIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCC4QgAQyFgguELEDEIoFEJECENwEEN4EEOAEGAE6CwgAEIAEELEDEIMBOg4ILhCABBCxAxDHARDRAzoICAAQgAQQsQM6EwguEIoFEJECENwEEN4EEOAEGAE6CgguEIoFENQCEEM6DgguEIAEELEDEIMBENQCOhUILhCKBRDUAhBDENwEEN4EEOAEGAE6BwguEIoFEEM6DgguEIAEELEDEMcBEK8BOgsILhCABBCxAxCDAToLCC4QgAQQsQMQ1AI6CwguEIAEEMcBEK8BOgsILhCKBRCxAxCRAjoHCAAQigUQQzoKCC4QigUQsQMQQzoICC4QsQMQgAQ6CAguEIAEELEDOhYILhCKBRCxAxCRAhDcBBDeBBDgBBgBOgcILhCABBAKOgcIABCABBAKOhYILhCxAxCKBRCRAhDcBBDdBBDgBBgBSgQIQRgAUABYjEJg5EZoAXABeACAAW2IAaIJkgEEMTMuMpgBAKABAcABAdoBBggBEAEYFA&sclient=gws-wiz-serp) by Brené Brown: How to let go of fear and overcome apprehension 
- [The Infinite Game](https://www.google.com/search?q=the+infinite+game&rlz=1C5CHFA_enGB988GB988&ei=3dQ-ZMCtOIedgQao1KGgAw&gs_ssp=eJzj4tVP1zc0zDIoM6gsT8ozYPQSLMlIVcjMS8vMyyxJVUhPzE0FALkOCzw&oq=the+infinit&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQARgAMggILhCKBRCRAjIICC4QgAQQ1AIyCAguEIAEENQCMggILhCABBDUAjIFCC4QgAQyBQguEIAEMgUILhCABDIICC4Q1AIQgAQyBQgAEIAEMgUILhCABDITCC4QigUQkQIQ3AQQ3gQQ4AQYAToHCC4QigUQQzoKCC4QigUQ1AIQQzoHCAAQigUQQzoSCC4QigUQQxDcBBDeBBDgBBgBOgsILhCABBCxAxCDAToLCAAQgAQQsQMQgwE6CAgAEIAEELEDOgsILhCvARDHARCABDoLCC4QgAQQxwEQrwE6CwguEIAEELEDENQCOggILhCABBCxAzoRCC4QgAQQsQMQyQMQxwEQ0QM6CAgAEIAEEJIDOggIABCKBRCSAzoUCC4QgAQQsQMQgwEQxwEQ0QMQ1AJKBAhBGABQAFiJCWChG2gAcAF4AIABgQGIAe0HkgEDOS4ymAEAoAEBwAEB2gEGCAEQARgU&sclient=gws-wiz-serp) by Simon Sinek: The balance between thinking long term & short term 
- [What got you here, Won’t get you there](https://www.google.com/search?q=what+got+you+here&rlz=1C5CHFA_enGB988GB988&ei=CdU-ZOgelP-ABtLbvogI&gs_ssp=eJzj4tLP1TcwKU-vtCg3YPQSLM9ILFFIzy9RqMwvVchILUoFAJo6CjU&oq=what+got+you+&gs_lcp=Cgxnd3Mtd2l6LXNlcnAQARgAMgcILhCKBRBDMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCC4QgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMhIILhCKBRBDENwEEN4EEOAEGAE6CggAEEcQ1gQQsAM6CgguEIoFELADEENKBAhBGABQ1gRY1gRgwxtoAXABeACAATyIATySAQExmAEAoAEByAEJwAEB2gEGCAEQARgU&sclient=gws-wiz-serp): How to think about evolution in yourself 


**Impactful videos**
- Simon Sinek: [Start with Why](https://www.youtube.com/watch?v=u4ZoJKF_VuA&pp=ygUac2ltb24gc2luZWsgc3RhcnQgd2l0aCB3aHk%3D)
- Brené Brown: [Vulnerability](https://www.youtube.com/watch?v=iCvmsMzlF7o&pp=ygUZYnJlbmUgYnJvd24gdnVsbmVyYWJpbGl0eQ%3D%3D) 
- Admiral McRaven: [Start the day by making your bed](https://www.youtube.com/watch?v=yaQZFhrW0fU&pp=ygUebWFrZSB5b3VyIGJlZCBzcGVlY2ggbmF2eSBzZWFs)
- Simon Sinek: [Nervous vs Excited](https://www.youtube.com/watch?v=0SUTInEaQ3Q&pp=ygUec2ltb24gc2luZWsgbmVydm91cyB2cyBleGNpdGVk)


**A few things to know**
- I hate tardiness, but I do not mind being late myself (especially because it winds up my wife!)
- I always look upon everything I do with positivity. Be grateful you are able to do what you do. There are many not so fortunate
- It is easy to moan, it is not easy to drive yourself to change. Take ownership, take action and find the way through that you want for yourself. Once I found this, I began to accelerate in everything that mattered to me


When I am struggling to find my way, the passage that I refer to is “[Man in the Arena](https://www.mentalfloss.com/article/63389/roosevelts-man-arena)”  by Theodore Roosevelt.
This reminds me that my efforts, however successful (or not) will ensure I win just by participating where many others will not even start. 

## My Work here at GitLab

I look upon my role here at GitLab with great affection. This company is allowing me to explore my creative side within passion while supporting my tenacity for attainment. The following are a few thoughts I have on myself and my position within this company.

**Guiding Principles**
- Celebrate yourself and your achievements, always
- Focus on teamwork in everything you do
- Turn the screw, ensure everything is aligned at all times. Where it is not, work to align it
 

**My Work**

For a view of the GitLab projects I am involved in please consult [my Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/5007892). (Re above: Any guidance on how to best organise this, all ideas welcome!)

**My Personal Plan** 


- The [Personal Development Plan](https://drive.google.com/file/d/1-CLcVFGKD26gRY_J2EMfEhN6KRXAZZL3/view?usp=sharing) I held at HPE, my previous employer 
- The [Individual Growth Plan](https://docs.google.com/document/d/1J845vR2cT3r1QWbOAH78VHoczcN4OjyniBEhOn2Eyag/edit#) I hold here at GitLab, which is a continued WIP

**My Vision**

1. Setting up an environment that enables everyone within it to know their self value and see the results of the work they undertake. Quite literally, A World where everyone contributes
1. The mindset of an individual is one challenged, educated and happy. If this can be achieved we will be unstoppable 
1. Everything we create is not just to enable success today but paves a path for the future ‘us’ to attain even more than we have, in a more efficient and effective manner 
 

**My perceived Value**
1. Business: Guiding the growth of the SMB business across every facet that requires attention, in order of importance, for the now with a keen eye on the future (Finite vs Infinite thinking) 
1. People: Creating an environment within SMB for the greatest group of Friends to successfully operate within shall allow everyone to fulfill their potential, as long as they focus and engage back
1. Living with Positive intent: Not accepting what is in place today is right for the future. If we become accepting of our position, the complacency will halt our progress


**What will I be doing to contribute to my Vision**
1. Hold discipline in data: gain context from data points to triangulate an opinion that is based on fact and emotion
1. First hand participation: Grow within the business and not watch it do so around me. Knowing one level up and two levels down will ensure the right decisions can be made at all times
1. Partnership: Being aware of the macro environment will ensure you can partner for success both internally and externally


**How are we doing this**: (no particular order - projects within board relate to this)
1. Hold discipline in Data
- Individual awareness of their actions against their outcome 
- Predictability through trending of performance 
- Effective Tooling to maintain quality
 
 

2. First hand participation
- Increasing engagement against the Diversity, Inclusion & Belonging TMRG within GitLab 
- Maintaining brand consistency in how we promote ourselves
- Effective meeting hosting: Looking into the way we coach a customer through their GitLab journey 
- What is your Superpower


3.Partnership
- Project Eleanor: What does the optimum customer journey look like 
- Increasing engagement with GitLab across the reseller community 
- The Account Executive onboarding experience 
- The ASM onboarding experience
- Internal career development planning: BDR to AE and SMB to MM, Management, Channel, Alliance
- Coach the Coach

 


**The SMB EMEA Business Plan** (Currently a WIP) 

This [plan is a working document](https://docs.google.com/document/d/1Ca3sZ2M9OFjR1YAzRFeADIr7kqEuMuy2vHdmz5-loAc/edit) focussed on the individual elements of the set up of the region and business unit. 
This plan will be categorised against the following five treatment models for action:
 
- Planned Infinite Strategy 
- Planned Finite Strategy
- Proactive Tactics
- Reactive Tactics
- Point in time actions 


Thank you for reading, I welcome feedback, questions and as always improvements. 
Together we will make GitLab great. 
